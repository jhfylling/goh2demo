import React, {Component} from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import Axios from 'axios';

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src="test.jpg" alt="test image with no meaning" width="60%" height="auto"/>
                    <img src="test.jpg" alt="test image with no meaning" width="60%" height="auto"/>
                    <Button variant="contained" color="primary" onClick={this.getImportantInfo}>
                        Get very important info
                    </Button>
                    <Button variant="contained" color="primary" onClick={this.sendPostStuff}>
                        Send important message
                    </Button>
                </header>
            </div>
        );
    }

    getImportantInfo() {
        Axios.get('https://localhost/important')
            .then(res => {
                console.log(res)
            })
    }

    sendPostStuff(){
        Axios.post('https://localhost/importantpost', 'posted hello')
            .then(res => {
                console.log(res)
            })
    }

}

export default App;
