package main

import (
	"fmt"
	"gitlab.com/jhfylling/gohttp2"
)

func main(){
	//static.Listen()

	//Paths for test purposes
	//rootPath := "/home/johan/goh2demo/devh2/" //ubuntu
	rootPath := "C:/Users/pyrof/go/src/goh2demo/" //laptop

	address := "localhost:443"


	app := goh2.NewApp(rootPath+ "devh2/build", rootPath+ "server.key", rootPath+ "server.crt", false)

	app.SetServeStatic(true)
	//app.SetPublicFolder("D:/Projects/go/src/gohttp2/frontend/devh2/build")

	app.Get("/important/", func(req goh2.Request, res goh2.Response){
		fmt.Println("GET /important/")

		jsonTest := "{name: \"Johan\", age: 31, city: \"New York\", message: \"very important\"}"

		err := res.Send([]byte(jsonTest), "text/json")
		if err != nil{
			fmt.Println(err)
		}
	})

	//TODO: Seems like path doesn't matter. Investigate.
	app.Post("/importantpost", func(request goh2.Request, res goh2.Response){
		fmt.Println("POST /importantpost/")

		fmt.Println("POST data: ", string(request.Data))

		err := res.Send([]byte("Hello poster"), "text/plain")
		if err != nil{
			fmt.Println(err)
		}
	})


	goh2.Listen(address, &app) //address:port, app

}
